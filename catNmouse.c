///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Bodie Collins <Bodie@hawaii.edu> 
/// @date    23 Jan 2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

const int DEFAULT_MAX_NUMBER = 2048;


int main( int argc, char* argv[] ) 
{
   int theMaxValue = DEFAULT_MAX_NUMBER;
  
   // if new max number is entered when run
   if(argc >=2)
   {
      int newMax = atoi(argv[1]);
      
      //error if less than 1 argument is entered
      if( newMax < 1)
      {
         printf("The argument must be larger or equal to 1\n");
         exit(1);
      }
      theMaxValue=newMax;
   }
    
   //randomize rand seed
   srand(time(0));
      
   //pick a random number from 1 to max number
   int num=(rand() % theMaxValue) + 1 ;
   
   printf("number is %d\n",num);


   // run until correct guess
   do
   {
      printf("OK cat,I'm thinking of a number from 1 to %d.\n Make a Guess.",theMaxValue);
      int guess;
      scanf("%d",&guess);

         //if guess is less than 1
         if(guess<1 )
         {
            printf("You must enter a number thats greater or equal to 1\n");
            continue;
         }

         //if guess is greater than max number
         if(guess>theMaxValue)
         {
            printf("You must enter a number less than %d \n",theMaxValue);
            continue;
         }

         //if guess is vaild check if its correct of lower or higher
         else
         {
         // if correcrt guess
            if(guess == num)
               {
                  printf("YOU GOT ME !!\n");
                  printf("  /|_____/|\n");
                  printf(" /  o   o  )\n");
                  printf("( ==  ^  == )\n");
                  printf(" )         (\n");
                  printf("(           )\n");
                  printf("( (  )   (  ) )\n");
                  printf("(__(__)___(__)__)\n");
                  break;
                  exit(0);
               }
               //if guess to high
               if(guess > num)
               {
                  printf("No cat... the number im thinking of is smaller than %d \n",guess);
                  continue;
               }
               //if guess to low
               if(guess< num)
               {
                  printf("No cat... the number im thinking of is larger than %d \n",guess);
                  continue;
               }
            }
      }while(true);
}

